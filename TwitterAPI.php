<?php
/**
 * @author Rabin Shrestha <rabinshr@gmail.com>
 * @date 23/04/14 12:51PM
 * @package PHP Twitter OAuth API
 * @link http://github.com/rabinshr *
 */

class TwitterAPI {
    private $tokens = array();

    public function setAccessToken($access_token) {
        $this->tokens['access_token'] = $access_token;
    }

    public function getAccessToken() {
        return $this->tokens['access_token'];
    }

    public function setAccessTokenSecret($access_token_secret) {
        $this->tokens['access_token_secret'] = $access_token_secret;
    }

    public function getAccessTokenSecret() {
        return $this->tokens['access_token_secret'];
    }

    public function setConsumerKey($consumer_key) {
        $this->tokens['consumer_key'] = $consumer_key;
    }

    public function getConsumerKey() {
        return $this->tokens['consumer_key'];
    }

    public function setConsumerSecret($consumer_secret) {
        $this->tokens['consumer_secret'] = $consumer_secret;
    }

    public function getConsumerSecret() {
        return $this->tokens['consumer_secret'];
    }

    public function __construct() {
        if (!in_array('curl', get_loaded_extensions())) {
            throw new Exception('You need to install cURL, see: http://curl.haxx.se/docs/install.html');
        }
    }

    public function GetUserInfo($params = array(), $fields = array()) {
        $r = array();
        $url = "users/show.json";
        $info = $this->sendRequest($url, $params);

        if (!empty($info)) {
            $info = json_decode($info);

            foreach ($fields as $f) {
                $r[$f] = $info->$f;
            }
        }
        return $r;
    }

    protected function sendRequest($path, $params = array(), $post = array()) {
        $oAuthURL = "https://api.twitter.com/1.1/";
        $url = $oAuthURL.$path;
        $requestMethod = 'GET';
        if (!empty($post)) {
            $requestMethod = 'POST';
        }

        $headers = array($this->buildAuthorizationHeader($this->buildOAuth($url, $requestMethod, $params)), 'Expect:');

        $options = array(
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
        );

        $options[CURLOPT_URL] = $url;
        if (!empty($params)) {
            $options[CURLOPT_URL] .= '?'.http_build_query($params);
        }

        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        curl_close($feed);

        return $json;
    }

    protected function getCompositeKey($consumerSecret, $requestToken) {
        return rawurlencode($consumerSecret) . '&' . rawurlencode($requestToken);
    }

    protected function buildOAuth($url, $requestMethod, $params) {
        $oauth = array(
            'oauth_consumer_key' => $this->getConsumerKey(),
            'oauth_nonce' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token' => $this->getAccessToken(),
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0'
        );
        $oauth = array_merge($oauth, $params);

        $base_info = $this->buildBaseString($url, $requestMethod, $oauth);
        $composite_key = $this->getCompositeKey($this->getConsumerSecret(), $this->getAccessTokenSecret());
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;

        return $oauth;
    }

    private function buildBaseString($baseURI, $method, $params) {
        ksort($params);
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(http_build_query($params));
    }

    private function buildAuthorizationHeader($oauth) {
        $return = 'Authorization: OAuth ';
        $values = array();

        foreach ($oauth as $key => $value) {
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        }

        $return .= implode(', ', $values);
        return $return;
    }
}