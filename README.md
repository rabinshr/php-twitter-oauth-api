php-twitter-oauth-api
=====================
Simple PHP Wrapper for Twitter API v1.1 oauth calls

Installation
------------

**Via Composer:**:

    {
        "require": {
            "rabinshr/php-twitter-oauth-api": "dev-master"
        }
    }

And then run `php composer.phar update`.

How To Use
----------
#### Include the class file ####

    require_once('TwitterAPI.php');

#### Initialise a new twitter object ####

    $twitter = new TwitterAPI();

#### Set twitter app tokens ####

    $twitter->setAccessToken("TOKEN HERE");
    $twitter->setAccessTokenSecret("TOKEN SECRET");
    $twitter->setConsumerKey("CONSUMER KEY");
    $twitter->setConsumerSecret("CONSUMER KEY SECRET");

#### Setup additional params ####

    $params = array('screen_name' => 'SCREEN_NAME',
                'userid' => 'USERID');

#### Indicate the fields you want returned ####

    $fields = array('followers_count', 'friends_count', 'statuses_count');

#### Get User Info ####

    $info = $twitter->GetUserInfo($params, $fields);

Helpful links http://hannah.wf/twitter-oauth-simple-curl-requests-for-your-own-data/