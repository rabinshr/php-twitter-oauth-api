<?php

include 'TwitterAPI.php';

$twitter = new TwitterAPI();
$twitter->setAccessToken("TOKEN HERE");
$twitter->setAccessTokenSecret("TOKEN SECRET");
$twitter->setConsumerKey("CONSUMER KEY");
$twitter->setConsumerSecret("CONSUMER KEY SECRET");

$params = array('screen_name' => 'SCREEN_NAME',
                'userid' => 'USERID');
$fields = array('followers_count', 'friends_count', 'statuses_count');

$info = $twitter->GetUserInfo($params, $fields);
var_dump($info);
